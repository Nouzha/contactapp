import { Contact } from './../Contact';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import { ContactAccessService } from '../services/contact-access.service';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-liste-contact',
  templateUrl: './liste-contact.page.html',
  styleUrls: ['./liste-contact.page.scss'],
})
export class ListeContactPage implements OnInit {
  contacts: Contact[] = [];
  allContacts: Contact[] = [];
  searchString = new FormControl('');

  constructor(
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private contactService: ContactAccessService
  ) {
    this.contactService.getContacts().subscribe((res) => {
      console.log(res);
      this.contacts = res;
      this.allContacts = res;
    });
    this.menuCtrl.enable(true);
  }

  ngOnInit() {
    this.searchString.valueChanges.subscribe((value: string) => {
      if (value) {
        value = value.toLowerCase();
        this.contacts = this.allContacts.filter((contact) => {
          return (
            contact.nom.toLowerCase().includes(value) ||
            contact.prenom.toLowerCase().includes(value) ||
            contact.service.toLowerCase().includes(value)
          );
        });
      } else {
        this.contacts = this.allContacts;
      }
    });
  }

  ajouterContact() {
    this.navCtrl.navigateForward('/ajouter-contact');
  }
  openContact(contact) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        contact: contact,
      },
    };
    this.navCtrl.navigateForward('/detail-contact', navigationExtras);
  }
}
