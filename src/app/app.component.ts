import { AfterViewInit, Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { ContactAccessService } from './services/contact-access.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  public appPages = [
    { title: 'Mes contacts', url: '/liste-contact', icon: 'people' },
    { title: 'Recommandations', url: '/recommandations', icon: 'heart' },
    { title: 'Profile', url: '/profile', icon: 'person' },
    { title: 'Déconnexion', url: '/deconnexion', icon: 'log-out' },
  ];
  compte: any = {};
  constructor(
    private fireauth: AngularFireAuth,
    private contactService: ContactAccessService,
    private platform: Platform,
    private router: Router
  ) {
    this.fireauth.authState.subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.contactService
          .getComptesById(res.multiFactor.user.uid)
          .subscribe((user) => {
            this.compte = user;
          });
      }
    });
  }

  ngAfterViewInit() {
    this.platform.backButton.subscribe(() => {
      if (
        (this.router.isActive('/liste-contact', true) &&
          this.router.url === '/liste-contact') ||
        (this.router.isActive('/recommandations', true) &&
          this.router.url === '/recommandations') ||
        (this.router.isActive('/profile', true) &&
          this.router.url === '/profile')
      ) {
        navigator['app'].exitApp();
      }
    });
  }
}
