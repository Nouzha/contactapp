import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./authentification/authentification.module').then(
        (m) => m.AuthentificationPageModule
      ),
  },
  {
    path: 'ajouter-contact',
    loadChildren: () =>
      import('./ajouter-contact/ajouter-contact.module').then(
        (m) => m.AjouterContactPageModule
      ),
  },
  {
    path: 'authentification',
    loadChildren: () =>
      import('./authentification/authentification.module').then(
        (m) => m.AuthentificationPageModule
      ),
  },
  {
    path: 'deconnexion',
    loadChildren: () =>
      import('./deconnexion/deconnexion.module').then(
        (m) => m.DeconnexionPageModule
      ),
  },
  {
    path: 'detail-contact',
    loadChildren: () =>
      import('./detail-contact/detail-contact.module').then(
        (m) => m.DetailContactPageModule
      ),
  },
  {
    path: 'inscription',
    loadChildren: () =>
      import('./inscription/inscription.module').then(
        (m) => m.InscriptionPageModule
      ),
  },
  {
    path: 'liste-contact',
    loadChildren: () =>
      import('./liste-contact/liste-contact.module').then(
        (m) => m.ListeContactPageModule
      ),
  },
  {
    path: 'recommandations',
    loadChildren: () =>
      import('./recommandations/recommandations.module').then(
        (m) => m.RecommandationsPageModule
      ),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile/profile.module').then((m) => m.ProfilePageModule),
  },
  {
    path: 'detail-recommandations',
    loadChildren: () =>
      import('./detail-recommandations/detail-recommandations.module').then(
        (m) => m.DetailRecommandationsPageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
