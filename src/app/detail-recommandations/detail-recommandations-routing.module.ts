import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailRecommandationsPage } from './detail-recommandations.page';

const routes: Routes = [
  {
    path: '',
    component: DetailRecommandationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailRecommandationsPageRoutingModule {}
