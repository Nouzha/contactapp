import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailRecommandationsPageRoutingModule } from './detail-recommandations-routing.module';

import { DetailRecommandationsPage } from './detail-recommandations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailRecommandationsPageRoutingModule
  ],
  declarations: [DetailRecommandationsPage]
})
export class DetailRecommandationsPageModule {}
