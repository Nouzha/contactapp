import { Contact } from './../Contact';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  AlertController,
  NavController,
  ToastController,
} from '@ionic/angular';
import { ContactAccessService } from '../services/contact-access.service';

@Component({
  selector: 'app-detail-recommandations',
  templateUrl: './detail-recommandations.page.html',
  styleUrls: ['./detail-recommandations.page.scss'],
})
export class DetailRecommandationsPage implements OnInit {
  contact: Contact;

  constructor(
    private route: ActivatedRoute,
    private alertController: AlertController,
    private contactService: ContactAccessService,
    private toastController: ToastController,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.contact = params['contact'];
    });
  }

  async deleteFromRecommandations() {
    const confirmAlert = await this.alertController.create({
      header:
        'Voulez vous vraiment supprimer ce contact de la liste des recommandations ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            this.contactService
              .updateContact({ ...this.contact, isShared: false })
              .then(async (res) => {
                const toast = await this.toastController.create({
                  message: 'Contact supprimé avec succés!',
                  duration: 2000,
                  color: 'success',
                });
                toast.present();
                this.navCtrl.navigateBack('/recommandations');
              })
              .catch((err) => {
                alert(err);
              });
          },
        },
        {
          text: 'Annuler',
          handler: () => {},
        },
      ],
    });
    await confirmAlert.present();
  }
}
