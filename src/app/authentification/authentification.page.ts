import { Component, OnInit } from '@angular/core';
import {
  ReactiveFormsModule,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import {
  RouterOutlet,
  Router,
  ActivationStart,
  NavigationExtras,
} from '@angular/router';
import { Compte } from '../Compte';
import { NavController, MenuController } from '@ionic/angular';
import { ContactAccessService } from '../services/contact-access.service';
import { FirebaseAuthService } from '../services/firebase-auth.service';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.page.html',
  styleUrls: ['./authentification.page.scss'],
})
export class AuthentificationPage implements OnInit {
  authForm: FormGroup;
  constructor(
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private contactService: ContactAccessService,
    private fireauth: FirebaseAuthService,
    private formBuilder: FormBuilder
  ) {
    this.menuCtrl.enable(false);
  }
  ngOnInit() {
    this.authForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });
  }
  signIn() {
    this.fireauth.signIn(this.authForm.value).then(
      (res) => {
        console.log(res);
        this.navCtrl.navigateForward('/liste-contact');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  signUp() {
    this.navCtrl.navigateForward('/inscription');
  }
}
