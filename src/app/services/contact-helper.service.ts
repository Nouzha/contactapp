import { Injectable } from '@angular/core';
import { Validators, FormBuilder, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ContactHelper {
  constructor(private formBuilder: FormBuilder) {}
  initContactForm() {
    return this.formBuilder.group({
      id: new FormControl('', []),
      prenom: new FormControl('', [Validators.required]),
      nom: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      tel: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      service: new FormControl('', [Validators.required]),
      isShared: new FormControl('', []),
    });
  }
  initCompteForm() {
    return this.formBuilder.group({
      id: new FormControl('', []),
      prenom: new FormControl('', [Validators.required]),
      nom: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      tel: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }
}
