import {
  Component,
  OnInit,
  Inject,
  Injectable,
  InjectionToken,
} from '@angular/core';
import {
  Firestore,
  collectionData,
  docData,
  addDoc,
  deleteDoc,
  updateDoc,
} from '@angular/fire/firestore';
import { collection, doc } from '@firebase/firestore';
import { Observable, of, from } from 'rxjs';
import { Compte } from '../Compte';
import { Contact } from '../Contact';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root',
})
export class ContactAccessService {
  constructor(private firestore: Firestore, private afs: AngularFirestore) {}

  //Comptes

  getComptes(): Observable<Compte[]> {
    const compteRef = collection(this.firestore, 'Comptes');
    return collectionData(compteRef, { idField: 'id' }) as Observable<Compte[]>;
  }

  getComptesById(id): Observable<Compte> {
    const compteDocRef = doc(this.firestore, `Comptes/${id}`);
    return docData(compteDocRef, { idField: 'id' }) as Observable<Compte>;
  }

  addCompte(compte: Compte) {
    const compteRef = collection(this.firestore, 'Comptes');
    return addDoc(compteRef, compte);
  }

  addCompteById(compte: Compte) {
    return this.afs.collection<Compte>('Comptes').doc(compte.id).set(compte);
  }

  deleteCompte(compte: Compte) {
    const compteDocRef = doc(this.firestore, `Comptes/${compte.id}`);
    return deleteDoc(compteDocRef);
  }
  updateCompte(compte: Compte) {
    const compteDocRef = doc(this.firestore, `Comptes/${compte.id}`);
    return updateDoc(compteDocRef, {
      email: compte.email,
      nom: compte.nom,
      password: compte.password,
      prenom: compte.prenom,
      tel: compte.tel,
    });
  }

  //Contacts

  getContacts(): Observable<Contact[]> {
    const contactRef = collection(this.firestore, 'contacts');
    return collectionData(contactRef, { idField: 'id' }) as Observable<
      Contact[]
    >;
  }

  getContactsById(id): Observable<Contact> {
    const contactDocRef = doc(this.firestore, `contacts/${id}`);
    return docData(contactDocRef, { idField: 'id' }) as Observable<Contact>;
  }
  getContactsByEmail(email): Observable<Contact> {
    const contactDocRef = doc(this.firestore, `contacts/${email}`);
    return docData(contactDocRef, { idField: 'email' }) as Observable<Contact>;
  }

  addContact(contact: Contact) {
    const contactRef = collection(this.firestore, 'contacts');
    return addDoc(contactRef, contact);
  }
  deleteContact(contact: Contact) {
    const contactDocRef = doc(this.firestore, `contacts/${contact.id}`);
    return deleteDoc(contactDocRef);
  }
  updateContact(contact: Contact) {
    const contactDocRef = doc(this.firestore, `contacts/${contact.id}`);
    return updateDoc(contactDocRef, {
      adresse: contact.adresse,
      city: contact.city,
      email: contact.email,
      nom: contact.nom,
      prenom: contact.prenom,
      service: contact.service,
      tel: contact.tel,
      isShared: contact.isShared,
    });
  }

  //recommandations
  getRecommands(): Observable<Contact[]> {
    return this.afs
      .collection('contacts', (ref) => ref.where('isShared', '==', true))
      .valueChanges({ idField: 'id' }) as Observable<Contact[]>;
  }
}
