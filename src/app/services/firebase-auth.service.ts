import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root',
})
export class FirebaseAuthService {
  constructor(private fireauth: AngularFireAuth) {}

  signUp(value: any) {
    return new Promise<any>((resolve, reject) => {
      this.fireauth
        .createUserWithEmailAndPassword(value.email, value.password)
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
    });
  }

  signIn(value: any) {
    return new Promise<any>((resolve, reject) => {
      this.fireauth
        .signInWithEmailAndPassword(value.email, value.password)
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
    });
  }

  userDetails() {
    return this.fireauth.user;
  }

  signOut() {
    return new Promise((resolve, reject) => {
      if (this.fireauth.currentUser) {
        this.fireauth.signOut().then(
          (res) => resolve(res),
          (err) => reject(err)
        );
      }
    });
  }
}
