import { Component, OnInit } from '@angular/core';
import { ContactAccessService } from '../services/contact-access.service';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import {
  ReactiveFormsModule,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-deconnexion',
  templateUrl: './deconnexion.page.html',
  styleUrls: ['./deconnexion.page.scss'],
})
export class DeconnexionPage implements OnInit {
  constructor(
    private navCtrl: NavController,
    private fireauth: FirebaseAuthService
  ) {}

  ngOnInit() {}
  signOut() {
    this.fireauth.signOut().then(
      (res) => {
        this.navCtrl.navigateRoot('/authentification');
        console.log(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  back() {
    this.navCtrl.navigateRoot('/liste-contact');
  }
}
