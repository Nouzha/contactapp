import { Component, OnInit } from '@angular/core';
import { Compte } from '../Compte';
import { ContactAccessService } from '../services/contact-access.service';

import { FirebaseAuthService } from '../services/firebase-auth.service';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { ContactHelper } from '../services/contact-helper.service';
import { FormGroup } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  prfForm: FormGroup;
  image: String;
  compte: any = {};
  email: string;
  showPassword = false;
  passwordIcon = 'eye';

  constructor(
    private fireauth: AngularFireAuth,
    private contactService: ContactAccessService,
    private alertController: AlertController,
    private helper: ContactHelper,
    private navCtrl: NavController
  ) {
    this.prfForm = this.helper.initCompteForm();
    this.fireauth.authState.subscribe((res: any) => {
      console.log(res);
      this.contactService
        .getComptesById(res.multiFactor.user.uid)
        .subscribe((user) => {
          this.compte = user;
          this.prfForm.patchValue(this.compte);
          this.prfForm.disable();
        });
    });
  }

  ngOnInit() {}

  enableInput() {
    this.prfForm.enable();
  }

  async Save() {
    this.contactService.updateCompte(this.prfForm.value).then(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      }
    );
    const alert = await this.alertController.create({
      header: 'Votre profil a été modifié avec succès !',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok');
          },
        },
      ],
    });
    await alert.present();
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;
    if (this.passwordIcon == 'eye') {
      this.passwordIcon = 'eye-off';
    } else {
      this.passwordIcon = 'eye';
    }
  }
}
