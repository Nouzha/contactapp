export interface Contact {
  id?: string;
  prenom: string;
  nom: string;
  email: string;
  tel: string;
  city: string;
  adresse: string;
  service: string;
  isShared?: boolean;
  image?: string;
}
