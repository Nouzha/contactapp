import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { ContactAccessService } from '../services/contact-access.service';
import { Contact } from '../Contact';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-recommandations',
  templateUrl: './recommandations.page.html',
  styleUrls: ['./recommandations.page.scss'],
})
export class RecommandationsPage implements OnInit {
  recommands: Contact[] = [];
  allRecommands: Contact[] = [];
  searchString = new FormControl('');

  constructor(
    private contactService: ContactAccessService,
    private navCtrl: NavController
  ) {
    this.contactService.getRecommands().subscribe((res) => {
      console.log(res);
      this.recommands = res;
      this.allRecommands = res;
    });
  }

  ngOnInit() {
    this.searchString.valueChanges.subscribe((value: string) => {
      if (value) {
        value = value.toLowerCase();
        this.recommands = this.allRecommands.filter((contact) => {
          return (
            contact.nom.toLowerCase().includes(value) ||
            contact.prenom.toLowerCase().includes(value) ||
            contact.service.toLowerCase().includes(value)
          );
        });
      } else {
        this.recommands = this.allRecommands;
      }
    });
  }

  openContact(contact: Contact) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        contact: contact,
      },
    };
    this.navCtrl.navigateForward('/detail-recommandations', navigationExtras);
  }
}
