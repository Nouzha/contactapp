import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecommandationsPageRoutingModule } from './recommandations-routing.module';

import { RecommandationsPage } from './recommandations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RecommandationsPageRoutingModule,
  ],
  declarations: [RecommandationsPage],
})
export class RecommandationsPageModule {}
