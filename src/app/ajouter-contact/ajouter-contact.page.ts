import { Component, OnInit } from '@angular/core';
import {
  ReactiveFormsModule,
  Validators,
  FormGroup,
  FormsModule,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { ContactAccessService } from '../services/contact-access.service';
import { LoadingController, NavController } from '@ionic/angular';
import { Contact } from '../Contact';
import { ContactHelper } from '../services/contact-helper.service';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/compat/storage';
import { EMPTY, Observable } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-ajouter-contact',
  templateUrl: './ajouter-contact.page.html',
  styleUrls: ['./ajouter-contact.page.scss'],
})
export class AjouterContactPage implements OnInit {
  addForm: FormGroup;
  contacts: Contact[] = [];
  ngFireUploadTask: AngularFireUploadTask;
  fileUploadedPath: Observable<string>;
  file: any;

  constructor(
    private navCtrl: NavController,
    private contactService: ContactAccessService,
    private helper: ContactHelper,
    private angularFireStorage: AngularFireStorage,
    public loadingController: LoadingController
  ) {
    this.addForm = this.helper.initContactForm();
  }

  ngOnInit() {}

  async addContact() {
    const loading = await this.loadingController.create({
      message: 'Ajout en cours...',
    });
    await loading.present();
    const fileStoragePath = `filesStorage/${new Date().getTime()}_${
      this.file.name
    }`;
    const imageRef = this.angularFireStorage.ref(fileStoragePath);
    this.ngFireUploadTask = this.angularFireStorage.upload(
      fileStoragePath,
      this.file
    );
    this.ngFireUploadTask.then((res) => {
      this.fileUploadedPath = imageRef.getDownloadURL();
      this.fileUploadedPath.subscribe(
        (filepath) => {
          this.contactService
            .addContact({ ...this.addForm.value, image: filepath })
            .then(
              (res) => {
                console.log(res);
                loading.dismiss();
                this.navCtrl.navigateForward('/liste-contact');
              },
              (err) => {
                console.log(err);
              }
            );
        },
        (error) => {
          console.log(error);
        }
      );
    });
  }

  fileUpload(event: any) {
    const file = event.files[0];

    if (file.type.split('/')[0] !== 'image') {
      console.log('File type is not supported!');
      return;
    }
    this.file = file;
  }
}
