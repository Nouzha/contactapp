import { Contact } from './../Contact';
import { Component, OnInit } from '@angular/core';
import { ContactAccessService } from '../services/contact-access.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { ContactHelper } from '../services/contact-helper.service';
import { CallNumber } from '@awesome-cordova-plugins/call-number/ngx';
import { SMS } from '@awesome-cordova-plugins/sms/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-detail-contact',
  templateUrl: './detail-contact.page.html',
  styleUrls: ['./detail-contact.page.scss'],
})
export class DetailContactPage implements OnInit {
  newForm: FormGroup;
  contact: Contact;

  constructor(
    private contactService: ContactAccessService,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private alertController: AlertController,
    private helper: ContactHelper,
    private callNumber: CallNumber,
    private sms: SMS,
    private socialSharing: SocialSharing,
    private toastController: ToastController
  ) {
    this.newForm = this.helper.initContactForm();
    this.route.queryParams.subscribe((params) => {
      this.contact = params['contact'];
      this.newForm.patchValue(this.contact);
      this.newForm.disable();
      console.log(this.contact);
    });
  }
  ngOnInit() {}

  Modifier() {
    this.newForm.enable();
  }
  Sauvegarder() {
    this.contactService.updateContact(this.newForm.value).then(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async Supprimer() {
    const confirmAlert = await this.alertController.create({
      header: 'Voulez vous vraiment supprimer ce contact ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            this.contactService
              .deleteContact(this.contact)
              .then(async (res) => {
                const toast = await this.toastController.create({
                  message: 'Contact supprimé avec succés!',
                  duration: 2000,
                  color: 'success',
                });
                toast.present();
                this.navCtrl.navigateBack('/liste-contact');
              })
              .catch((err) => {
                alert(err);
              });
          },
        },
        {
          text: 'Annuler',
          handler: () => {
            console.log('Ok');
          },
        },
      ],
    });
    await confirmAlert.present();
  }
  async Partager() {
    if (this.newForm.value.isShared) {
      const alert = await this.alertController.create({
        header: 'Ce contact a été déjà partagé !',
        buttons: [
          {
            text: 'Ok',
          },
        ],
      });
      await alert.present();
      return;
    }
    this.contactService
      .updateContact({ ...this.newForm.value, isShared: true })
      .then(
        (res) => {
          console.log(res);
        },
        (err) => {
          console.log(err);
        }
      );
    const alert = await this.alertController.create({
      header: 'Votre contact est partagé avec succés !',
      buttons: [
        {
          text: 'Ok',
        },
      ],
    });
    await alert.present();
    this.navCtrl.navigateForward('/recommandations');
  }

  Appel() {
    this.callNumber
      .callNumber(this.contact.tel, true)
      .then((res) => console.log('Launched call!', res))
      .catch((err) => console.log('Error launching call', err));
  }
  sendSMS() {
    const options = {
      replaceLineBreaks: false,
      android: {
        intent: 'INTENT',
      },
    };
    this.sms
      .send(this.contact.tel.toString(), '', options)
      .then(() => {})
      .catch((err) => {
        alert(err);
      });
  }

  share() {
    this.socialSharing.share(
      `Vous pouvez contacter: ${this.contact.nom} ${this.contact.prenom} sur le numéro de téléphone: ${this.contact.tel}, ou l'email: ${this.contact.email}`
    );
  }
  email() {
    this.socialSharing
      .shareViaEmail('', '', [this.contact.email])
      .then(() => {})
      .catch((err) => {
        alert(err);
      });
  }
}
