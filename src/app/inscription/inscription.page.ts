import { Component, OnInit } from '@angular/core';
import {
  ReactiveFormsModule,
  Validators,
  FormGroup,
  FormsModule,
  FormBuilder,
} from '@angular/forms';
import { ContactAccessService } from '../services/contact-access.service';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { LoadingController, NavController } from '@ionic/angular';
import { Compte } from '../Compte';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/compat/storage';
import { ContactHelper } from '../services/contact-helper.service';
import { EMPTY, Observable } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  inscriptionForm: FormGroup;
  comptes: Compte[] = [];
  ngFireUploadTask: AngularFireUploadTask;
  fileUploadedPath: Observable<string>;
  file: any;

  constructor(
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private contactService: ContactAccessService,
    private fireauth: FirebaseAuthService,
    private helper: ContactHelper,
    private angularFireStorage: AngularFireStorage,
    public loadingController: LoadingController
  ) {
    this.inscriptionForm = this.helper.initCompteForm();
  }

  ngOnInit() {}

  signUp() {
    this.fireauth.signUp(this.inscriptionForm.value).then(
      (res) => {
        console.log(res);
        this.contactService.addCompteById({
          ...this.inscriptionForm.value,
          id: res.user.uid,
        });
        this.navCtrl.navigateForward('/authentification');
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
