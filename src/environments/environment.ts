// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAN7paq87JJOjNAJrVFScOvCg-FZsVLq8M',
    authDomain: 'myapp-3ff2e.firebaseapp.com',
    projectId: 'myapp-3ff2e',
    storageBucket: 'myapp-3ff2e.appspot.com',
    messagingSenderId: '35848743715',
    appId: '1:35848743715:web:e7ec87bbac077f90e67a96',
    measurementId: 'G-HE4DGFE0G2',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
